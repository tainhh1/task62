package com.devcamp.task62.modal;

public class Drink {
  private int Stt;
  private String drinkCode;
  private String drinkName;
  private int price;
  private String dateCreate;
  private String dateUpdate;
  
  public Drink() {
    this.Stt = 1;
    this.drinkCode = "TRATAC";
    this.drinkName = "Trà tắc";
    this.price = 10000;
    this.dateCreate = "14/5/2021";
    this.dateUpdate = "14/5/2021";
  }

  public Drink(int paramStt, String paramDrinkCode, String paramDrinkName, int paramPrice) {
    super();
    Stt = paramStt;
    this.drinkCode = paramDrinkCode;
    this.drinkName = paramDrinkName;
    this.price = paramPrice;
    this.dateCreate = "14/5/2021";
    this.dateUpdate = "14/5/2021";
  }

  public int getStt() {
    return Stt;
  }

  public void setStt(int paramStt) {
    Stt = paramStt;
  }

  public String getDrinkCode() {
    return drinkCode;
  }

  public void setDrinkCode(String paramDrinkCode) {
    this.drinkCode = paramDrinkCode;
  }

  public String getDrinkName() {
    return drinkName;
  }

  public void setDrinkName(String paramDrinkName) {
    this.drinkName = paramDrinkName;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int paramPrice) {
    this.price = paramPrice;
  }

  public String getDateCreated() {
    return dateCreate;
  }

  public void setDateCreated(String paramDateCreated) {
    this.dateCreate = paramDateCreated;
  }

  public String getDateUpdated() {
    return dateUpdate;
  }

  public void setDateUpdated(String paramDateUpdated) {
    this.dateUpdate = paramDateUpdated;
  }
}
