package com.devcamp.task62.modal;

import java.util.ArrayList;

public class CCountry {
  private String countryCode;
  private String countryName;
  private ArrayList<CRegion> regions;

  public CCountry() {
    this.countryCode = "VN";
    this.countryName = "Việt Nam";
    this.regions = new ArrayList<CRegion>();
  }

  public CCountry(String paramCountryCode, String paramCountryName) {
    super();
    this.countryCode = paramCountryCode;
    this.countryName = paramCountryName;
    this.regions = new ArrayList<CRegion>();
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String paramCountryCode) {
    this.countryCode = paramCountryCode;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String paramCountryName) {
    this.countryName = paramCountryName;
  }

  public ArrayList<CRegion> getRegions() {
    return regions;
  }

  public void addRegion(CRegion paramRegion) {
    this.regions.add(paramRegion);
  }
}
