package com.devcamp.task62.modal;

public class CRegion {
  private int regionCode;
  private String regionName;

  public CRegion() {
    this.regionCode = 1;
    this.regionName = "Hà Nội";
  }

  public CRegion(int paramRegionCode, String paramRegionName) {
    super();
    this.regionCode = paramRegionCode;
    this.regionName = paramRegionName;
  }

  public int getRegionCode() {
    return regionCode;
  }

  public void setRegionCode(int regionCode) {
    this.regionCode = regionCode;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
}
