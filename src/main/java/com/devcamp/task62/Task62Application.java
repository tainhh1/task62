package com.devcamp.task62;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.devcamp")
public class Task62Application {

	public static void main(String[] args) {
		SpringApplication.run(Task62Application.class, args);
	}

}
