package com.devcamp.task62.controller;

import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.task62.modal.*;

@RestController
public class Controller {
  @CrossOrigin
  @GetMapping("/drinkList")
  public ArrayList<Drink> getDrinkList() {
    Drink traTac = new Drink();
    Drink coca = new Drink(2, "COCA", "Cocacola", 15000);
    Drink pepsi = new Drink(3, "PEPSI", "Pepsi", 15000);
    ArrayList<Drink> drinkList = new ArrayList<Drink>(Arrays.asList(traTac, coca ,pepsi));
    return drinkList;
  }
  
  @CrossOrigin
  @GetMapping("/regionList")
  public ArrayList<CRegion> getRegionList(@RequestParam(value = "countryCode", defaultValue = "" ) String countryCode) {
//    Create region
    CRegion hanoi = new CRegion();
    CRegion hcm = new CRegion(2, "Tp Hồ Chí Minh");
    CRegion danang = new CRegion(3, "Đà Nẵng");
    CRegion sydney = new CRegion(1, "Sydney");
    CRegion blueMountain = new CRegion(2, "Blue Mountains");
    CRegion murray = new CRegion(3, "Murray");
    CRegion newyork = new CRegion(1, "New York");
    CRegion cali = new CRegion(2, "California");
    CRegion texas = new CRegion(3, "Texas");
//    Create Country
    CCountry vietnam = new CCountry();
    vietnam.addRegion(hanoi);
    vietnam.addRegion(hcm);
    vietnam.addRegion(danang);
    CCountry autralia = new CCountry("AU", "Australia");
    autralia.addRegion(sydney);
    autralia.addRegion(blueMountain);
    autralia.addRegion(murray);
    CCountry us = new CCountry("US", "United State");
    us.addRegion(newyork);
    us.addRegion(cali);
    us.addRegion(texas);
    ArrayList<CCountry> listCountry = new ArrayList<CCountry>(Arrays.asList(vietnam, autralia, us));
    ArrayList<CRegion> filteredList = new ArrayList<CRegion>();
    if (countryCode != "") {
      for (CCountry country: listCountry) {
        if (country.getCountryCode().equals(countryCode)) {
          filteredList.addAll(country.getRegions());
        }
      }
    } else {
      filteredList = new ArrayList<CRegion>();
    }
    
    return filteredList;
  }
}
